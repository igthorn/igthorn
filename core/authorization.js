
const jwt = require('express-jwt')

module.exports = ({config, logger}) => {
  if (config.get('authorizationEnabled')) {
    logger.info('Authorization is enabled, service will require Bearer token')

    const secret = config.get('authorizationServicePublicKey')
    return jwt({
      secret,
      algorithms: ['RS256']
    })
  }

  logger.info('Authorization is disabled')
  return (req, res, next) => next()
}
