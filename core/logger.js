
const winston = require('winston')

const init = config => {
  return new winston.Logger({
    level: config.get('loggingLevel'),
    transports: [
      new (winston.transports.Console)()
    ]
  })
}

module.exports = { init }
