
const config = require('config')
const logger = require('./../core/logger').init(config)
const db = require('./db').connect(config, { logger })

const grpcHandler = require('./grpc/handlers').init({
  db
})
