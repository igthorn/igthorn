
const baseCustomEnvironmentalVariablesConfig = require('../../core/config/baseCustomEnvironmentVariables')

module.exports = {
  ...baseCustomEnvironmentalVariablesConfig,
  postgres: {
    user: 'POSTGRES_USER',
    host: 'POSTGRES_HOST',
    port: 'POSTGRES_PORT',
    password: 'POSTGRES_PASSWORD',
    database: 'POSTGRES_DATABASE'
  }
}
