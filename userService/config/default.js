const baseDefaultConfig = require('../../core/default')

module.exports = {
  ...baseDefaultConfig,
  port: 10010,
  postgres: {
    database: 'postgres',
    user: 'postgres',
    password: 'postgres',
    host: 'localhost',
    port: '5432'
  }
}
