
const Sequelize = require('sequelize')
const pg = require('pg')

// Fix to: "Error: Cannot find module 'pg-native'"
// More about: https://github.com/sequelize/sequelize/issues/3781
delete pg.native

async function connect (config, { logger }) {
  const sequelize = new Sequelize(
    config.get('postgres.database'),
    config.get('postgres.user'),
    config.get('postgres.password'),
    {
      dialect: 'postgres',
      host: config.get('postgres.host'),
      port: config.get('postgres.port'),
      define: {
        freezeTableName: true,
        timestamps: false
      },
      logging: logger,
      pool: {
        max: 5,
        min: 0,
        idle: 10000
      }
    }
  )

  try {
    await sequelize.authenticate()
  } catch (error) {
    logger.error('Unable to connect to the database:', error)
    throw new Error('Postgres is not available')
  }

  logger.info(
    'Connection to Postgres database has been established successfully.'
  )

  return sequelize
}

module.exports = { connect }
