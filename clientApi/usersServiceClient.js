
const grpc = require('grpc')

const protoPath = require('path').join(__dirname, '../shared/grpc')

const proto = grpc.load({ root: protoPath, file: 'usersService.proto' })

const init = config => {
  const service = new proto.users_domain.UsersService(
    config.get('usersServiceAddress'),
    grpc.credentials.createInsecure()
  )

  return service
}

module.exports = {
  init
}
