
const config = require('config')
const { graphiqlExpress, graphqlExpress } = require('apollo-server-express')

const logger = require('./../core/logger').init(config)
const usersService = require('./usersServiceClient').init(config)
const jwtMiddleware = require('./../core/authorization')
const bodyParser = require('body-parser')

const express = require('express')
const app = express()
const schema = require('./graphql')

app.use(bodyParser.json());

app.use(
  '/query',
  jwtMiddleware({config, logger}),
  graphqlExpress(async () => {
    return {
      schema,
      context: { usersService, store: {} }
    }
  })
)

if (process.env.NODE_ENV === 'development') {
  app.use('/graphiql', graphiqlExpress({ endpointURL: '/query' }))
}

app.listen(config.get('port'))

logger.info(`Client api started at port ${config.get('port')}`);
