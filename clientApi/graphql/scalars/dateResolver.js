
module.exports = {
    __parseValue (value) {
        return value
    },
    __serialize (value) {
        return `${value.Year}-${(`${value.Month}`).padStart(2, '0')}-${(`${value.Day}`).padStart(2, '0')}`
    },
    __parseLiteral (ast) {
        if (ast.kind === Kind.INT) {
        return parseInt(ast.value, 10) // ast value is always in string format
        }

        return null
    }
};