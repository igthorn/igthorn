
const dateResolver = require('./scalars/dateResolver');

const loadResolvers = () => {
  const path = require("path").join(__dirname, "./../modules/");

  return require("fs").readdirSync(path).reduce((acc, file) => {
    return {
      ...acc,
      [file]: require(`${path}/${file}/resolver`)
    }
  }, {});
}

module.exports = {
  Query: {
    ...loadResolvers(),
  },
  Date: dateResolver,
};