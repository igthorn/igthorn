
const { makeExecutableSchema } = require('graphql-tools')

const typeDefs = require('fs').readFileSync('./graphql/schema.graphql', { encoding: 'utf8' })
const resolvers = require('./graphql/resolvers')

module.exports = makeExecutableSchema({ typeDefs, resolvers })
