
const baseCustomEnvironmentalVariablesConfig = require('../../core/config/baseCustomEnvironmentVariables')

module.exports = {
  ...baseCustomEnvironmentalVariablesConfig,
  usersServiceAddress: 'USERS_SERVICE_ADDRESS'
}
