
const baseDefaultConfig = require('../../core/config/baseDefault')

module.exports = {
  ...baseDefaultConfig,
  port: 10000,
  usersServiceAddress: 'localhost:10010',
  authorizationEnabled: false,
  authorizationServicePublicKey: '--fill-me-in--'
}
